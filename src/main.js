import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import moment from 'moment';
import axios from 'axios';
import * as restEndpoints from '@/constants/rest-endpoints';

Vue.config.productionTip = false;

// This event bus will be used to transfer the drawer state from
// TheToolbar component to TheNavigationDrawer component
export const eventBus = new Vue({
  methods: {
    toggleDrawer(drawer) {
      this.$emit('drawerWasChanged', drawer);
    },
  },
});

// add a global date filter
Vue.filter('date', (value) => {
  return moment(value).format('MMM D, YYYY');
});
Vue.filter('datetime', (value) => {
  return moment(value).format('MMM D, YYYY hh:mm:ss');
});

// configure the default instance of axios
axios.defaults.baseURL = restEndpoints.APP_URL;
axios.defaults.headers.common['Content-Type'] = 'application/json';

// Add a response interceptor to intercept all the error and extract the object representing the error from that
const instanceResponseInterceptor = axios.interceptors.response.use(
  function(config) {
    console.log('Axios-Response Interceptor : ', config);
    return config;
  },
  function(error) {
    let err = error.response ? error.response : error;
    return Promise.reject(err);
  }
);

// Eject the last interceptor. If you are in dev mode you can uncomment this line
axios.interceptors.response.eject(instanceResponseInterceptor);

// Add a request interceptor - use it only on development - we will disable it just after this instruction
const instanceRequestInterceptor = axios.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    console.log('Axios-Request Interceptor : ', config);
    return config;
  },
  function(error) {
    // Do something with request error
    console.log(
      'Axios Response Interceptor Error - There is an error : ',
      error
    );
    return Promise.reject(error);
  }
);

// Eject the last interceptor. If you are in dev mode you can uncomment this line
axios.interceptors.request.eject(instanceRequestInterceptor);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
