import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import Training from '@/views/Training.vue';
import MyTrainings from '@/views/UserTrainings';
import TrainingManage from '@/views/UserTrainings/TrainingManage.vue';
import TrainingCreate from '@/views/UserTrainings/TrainingCreate.vue';
import TrainingEdit from '@/views/UserTrainings/TrainingEdit.vue';
import TrainingPreview from '@/views/UserTrainings/TrainingPreview.vue';
import AdminPanel from '@/views/AdminPanel';
import UsersManagement from '@/views/AdminPanel/UsersManagement.vue';
import NewRequestsManagement from '@/views/AdminPanel/NewRequestsManagement.vue';
import TeamsManagement from '@/views/AdminPanel/TeamsManagement.vue';
import WhishList from '@/views/WhishList.vue';
import Profile from '@/views/Profile.vue';
import Settings from '@/views/Settings.vue';
import Author from '@/views/Author.vue';
import Signin from '@/views/Signin.vue';
import Signup from '@/views/Signup.vue';
import Signout from '@/components/TheSignout.vue';
import Resources from '@/views/Resources.vue';
import store from '@/store';
import * as routesNames from '@/constants/routes-names';

Vue.use(VueRouter);

// before entering any route we will check if we have a token otherwise we will redirect the user to the sign-in page
const checkAuthentication = (to, from, next) => {
  if (store.getters['auth/isAuthenticated']) {
    next();
  } else {
    next({ name: routesNames.SIGN_IN_PAGE });
  }
};

// redirect user to home if he is already authenticated
const redirectHome = (to, from, next) => {
  if (!store.getters['auth/isAuthenticated']) {
    next();
  } else {
    next({ name: routesNames.HOME_PAGE });
  }
};

const hasWriteUsersRole = (to, from, next) => {
  // only users with ROLE_USERS_WRITER permission can access the settings page and profile page
  if (store.getters['auth/hasWriteUsersPerm']) next();
  else next(false);
};

const hasReadUsersRole = (to, from, next) => {
  // only users with ROLE_USERS_READER permission can access users data like resources and trainings
  if (store.getters['auth/hasReadUsersPerm']) next();
  else next(false);
};

const canReadTrainings = (to, from, next) => {
  // show my trainings only if the user has the read permissions on trainings
  if (store.getters['auth/canReadTrainings']) next();
  else next(false);
};

const routes = [
  {
    path: '/',
    name: routesNames.HOME_PAGE,
    component: Home,
    beforeEnter: canReadTrainings,
  },
  {
    path: '/signin',
    name: routesNames.SIGN_IN_PAGE,
    component: Signin,
  },
  {
    path: '/signup',
    name: routesNames.SIGN_UP_PAGE,
    component: Signup,
  },
  {
    path: '/logout',
    name: routesNames.LOG_OUT_ROUTE,
    component: Signout,
  },
  {
    path: '/training/:id',
    name: routesNames.TRAINING_PAGE,
    component: Training,
    beforeEnter: canReadTrainings,
  },
  {
    path: '/user/trainings',
    name: routesNames.MY_TRAININGS_PAGE,
    component: MyTrainings,
    beforeEnter: (to, from, next) => {
      // show my trainings only if the user has the read/write permissions on trainings and has the permission to read user resources like trainings
      if (store.getters['auth/canManageTrainings']) next();
      else next(false);
    },
    children: [
      {
        path: '',
        component: TrainingManage,
        name: routesNames.MANAGE_TRAINING_PAGE,
      },
      {
        path: 'create',
        component: TrainingCreate,
        name: routesNames.CREATE_NEW_TRAINING_PAGE,
      },
      {
        path: ':id',
        component: TrainingPreview,
        name: routesNames.PREVIEW_TRAINING_PAGE,
      },
      {
        path: ':id/edit',
        component: TrainingEdit,
        name: routesNames.EDIT_TRAINING_PAGE,
      },
    ],
  },
  {
    path: '/admin',
    name: routesNames.ADMIN_PAGE,
    component: AdminPanel,
    beforeEnter: (to, from, next) => {
      // only admins have access to this area
      if (store.getters['auth/isAdmin']) next();
      else next(false);
    },
    children: [
      {
        path: 'users',
        component: UsersManagement,
        name: routesNames.ADMIN_USERS_PAGE,
      },
      {
        path: 'requests',
        component: NewRequestsManagement,
        name: routesNames.ADMIN_REQUESTS_PAGE,
      },
      {
        path: 'teams',
        component: TeamsManagement,
        name: routesNames.ADMIN_TEAMS_PAGE,
      },
    ],
  },
  {
    path: '/author/:uid',
    name: routesNames.AUTHOR_PAGE,
    component: Author,
    beforeEnter: (to, from, next) => {
      // only users with ROLE_USERS_READER permission can access users data like resources and trainings
      if (
        store.getters['auth/hasReadUsersPerm'] ||
        store.getters['auth/isAdmin']
      )
        next();
      else next(false);
    },
  },
  {
    path: '/about',
    name: routesNames.ABOUT_PAGE,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/whishlist',
    name: routesNames.WHISH_LIST_PAGE,
    component: WhishList,
    beforeEnter: hasReadUsersRole,
  },
  {
    path: '/settings',
    name: routesNames.SETTINGS_PAGE,
    component: Settings,
    beforeEnter: hasWriteUsersRole,
  },
  {
    path: '/profile',
    name: routesNames.PROFILE_PAGE,
    component: Profile,
    beforeEnter: hasWriteUsersRole,
  },
  {
    path: '/resources',
    name: routesNames.RESOURCES_PAGE,
    component: Resources,
    beforeEnter: hasReadUsersRole,
  },
  // some redirection
  { path: '/admin', redirect: { path: '/admin/users' } }, // TODO does not work ??
  // any other route will be redirected
  { path: '*', redirect: { path: '/' } },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

// https://router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards
router.beforeEach((to, from, next) => {
  // ignore the some routes otherwise we have to check the token before tolerating navigation
  if (
    to.name === routesNames.SIGN_IN_PAGE ||
    to.name === routesNames.SIGN_UP_PAGE
  )
    redirectHome(to, from, next);
  else checkAuthentication(to, from, next);
});

export default router;
