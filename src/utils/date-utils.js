import moment from 'moment';

/**
 * Returns true if the passed date is in the past
 * @param date date to check if it is in the past or not
 */
export function isInThePast(date) {
  var now = new Date();
  now.setHours(0, 0, 0, 0);
  return date < now;
}

/**
 * Returns the date of yesterday
 * @param date date to get yesterday
 */
export function yesterday(date) {
  return new Date(date - 86400000).toISOString().substr(0, 10); // that is: 24 * 60 * 60 * 1000
}

/**
 * returns the minimum age to user the app, 18 years old at least
 */
export function minimumAge() {
  // 10 years old at minimum
  return moment()
    .subtract(18, 'years')
    .format('YYYY-MM-D');
}

/**
 * returns the date after formatting it in a very long format , example : Sunday, December 22, 2019 8:43 PM
 */
export function veryLongDate(date) {
  // 10 years old at minimum
  return moment(date).format('LLLL');
}
