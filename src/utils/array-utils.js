/**
 * remove en element from an array
 * @param {primitive Object} element element to remove
 * @param {Array} array js Array
 */
export const removeElement = (element, array) => {
  if (!array || array.size === 0) return;
  let index = array.indexOf(element);
  if (index !== -1) {
    array.splice(index, 1);
  }
};

/**
 * returns elements from arrA that does not exists in arrB
 * source : https://medium.com/@alvaro.saburido/set-theory-for-arrays-in-es6-eb2f20a61848
 *
 * @param {*} arrA First array
 * @param {*} arrB Second array
 */
export const diff = (arrA, arrB) => {
  if (!arrA || arrA.size === 0) return [];
  if (!arrB || arrB.size === 0) return arrA;

  return arrA.filter((x) => !arrB.includes(x));
};
