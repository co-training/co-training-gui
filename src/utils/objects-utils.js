/**
 * Clone a simple js object
 * Use only this method for simple objects otherwise you will have a lot of issues : check this link to understand more
 * https://medium.com/javascript-in-plain-english/how-to-deep-copy-objects-and-arrays-in-javascript-7c911359b089
 * 
 * @param {Object to clone} nodesArray
 */
export function cloneSimpleObject(nodesArray) {
  if (!nodesArray) return null;
  return JSON.parse(JSON.stringify(nodesArray));
}
