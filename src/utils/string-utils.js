/**
 * Returns true if the passed email is a valid email, false otherwise
 * @param {email} the email to check
 */
export function isValidEmail(email) {
  return emailPattern.test(email);
}

const emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

/**
 * Returns true if the passed price is a valid one (precision 2), false otherwise
 * @param {price} the price to check
 */
export function isValidPrice(price) {
  let pricePattern = /^([0-9]+)(\.[0-9]{1,2})?$/;
  return pricePattern.test(price);
}

/**
 * Returns true if the passed value is a link
 * @param {link} the link to check
 */
export function isValidLink(link) {
  // https://www.regextester.com/
  const pattern = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
  return pattern.test(link);
}

/**
 * Returns true if the passed value is a french phone number
 * @param {phone} the phone to check
 */
export function isValidFrenchPhoneNumber(phone) {
  // https://www.regextester.com/
  let pattern = /^((\+)33|0)[1-9](\d{2}){4}$/;
  return pattern.test(phone);
}

/**
 * Returns true if the passed price is a valid number of days (starts with digit, after the comma we can specify zeo : day or five : half a day)
 * examples : 2 two days ; 3.0 three days ; 0.5 half a day
 * @param {days} the number of days to check
 */
export function isValidNbDays(days) {
  let daysPattern = /^([0-9]+)(\.[05]{1})?$/;
  return daysPattern.test(days);
}

/**
 * map the training level to its color
 *
 * @param {level} level the training level
 */
export function pickColor(level) {
  if (level === 'BEGINNER') return 'light-green lighten-2';
  else if (level === 'INTERMEDIATE') return 'amber darken-2';
  else if (level === 'ADVANCED') return 'red lighten-2';
}
