/*
 * this file contains methods used to transform some data from our application data to another data
 */

/**
 *  as spring roles starts with ROLE_ , we have to remove this part from the role name to show it to frontend users
 *  a ROLE_ANY_ROLE will be transformed to any-role
 *
 * @param {*} backRoles backend role
 */
export function fromBackToFrontRoles(backRoles) {
  if (!backRoles) return [];
  return backRoles.map((role) => {
    return role
      .substring(5)
      .toLowerCase()
      .replace('_', '-');
  });
}

/**
 * transform front end roles to backend roles
 *
 * @param {*} frontRoles frontend roles
 */
export function fromFrontToBackRoles(frontRoles) {
  if (!frontRoles) return [];
  return frontRoles
    .map((role) => {
      return 'role_' + role.replace(/-/g, '_');
    })
    .map((role) => role.toUpperCase());
}

/**
 * this is a function that can be used to transform a front role to back role
 *
 * @param {*} role role to transform
 */
export const fromFrontToBackRolesFunction = (role) =>
  ('role_' + role.replace(/-/g, '_')).toUpperCase();
