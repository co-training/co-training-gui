// This file contains methods to generate alerts in the app

/**
 * 
 * @param {message} the message to show 
 */
export function info(message) {
  
    return {
        status: true,
        message,
        type : 'info'
    }
}

/**
 * 
 * @param {err} the error to show 
 */
export function error(err) {
  
    return {
        status: true,
        message: parseError(err),
        type : 'error'
    }
}

const parseError = (err) => {
    if(!err.data)
        return `An error has occurred : ${err.toString()}`;
    
    if(err.data.message)
        return err.data.message
    else if (err.data.error)
        return err.data.error_description
}