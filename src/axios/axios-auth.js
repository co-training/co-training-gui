import axios from 'axios';
import * as restEndpoints from '@/constants/rest-endpoints';

// most of the times the authorization is not the same as the resource server so we will give each one in a separate config
const instance = axios.create({
  baseURL: restEndpoints.OAUTH_URL,
  timeout: 5000,
});

// Add a request interceptor
const instanceInterceptor = instance.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    console.log('Axios-AuthRequest Interceptor : ', config);
    return config;
  },
  function(error) {
    // Do something with request error
    console.log('Axios-Auth Request Interceptor - There is an error : ', error);
    return Promise.reject(error);
  }
);

// Eject the last interceptor. If you are in dev mode you can uncomment this line
axios.interceptors.request.eject(instanceInterceptor);

// Add a response interceptor to intercept all the error and extract the object representing the error from that
const instanceRequestInterceptor = instance.interceptors.response.use(
  function(config) {
    return config;
  },
  function(error) {
    let err = error.response ? error.response : error;
    return Promise.reject(err);
  }
);

// Eject the last interceptor. If you are in dev mode you can uncomment this line
axios.interceptors.response.eject(instanceRequestInterceptor);

export default instance;
