import axiosAuth from '@/axios/axios-auth';
import axios from 'axios';
import * as restEndpoints from '@/constants/rest-endpoints';
import { stringify } from 'querystring';
import router from '@/router';
import * as routesNames from '@/constants/routes-names';

const state = {
  token: null,
  uid: null, // this is the user id
  aid: null, // user account id
  roles: null, // the user roles
  userName: null, // the user name ( this data can be retrieve from the current_account service but we will not store all the response, only the data needed)
  photo: null, // user photo url
};

const mutations = {
  AUTH_USER(state, { token, uid, aid, roles, userName, photo }) {
    state.token = token;
    state.uid = uid;
    state.aid = aid;
    state.roles = roles;
    state.userName = userName;
    state.photo = photo;
  },
  CLEAR_USER_DATA(state) {
    state.token = null;
    state.uid = null;
    state.aid = null;
    state.roles = null;
    state.userName = null;
    state.photo = null;
  },
  SET_USER_NAME(state, { payload }) {
    state.userName = payload;
  },
  SET_USER_PHOTO(state, { payload }) {
    state.photo = payload;
  },
};

const actions = {
  SIGN_IN({ commit, dispatch }, payload) {
    // return a promise from the action , https://vuex.vuejs.org/guide/actions.html#composing-actions
    return axiosAuth({
      method: 'POST',
      data: stringify({
        username: payload.user.email,
        password: payload.user.password,
        grant_type: restEndpoints.OAUTH_GRANT_TYPE,
      }),
      auth: {
        username: restEndpoints.OAUTH_CLIENT_ID,
        password: restEndpoints.OAUTH_CLIENT_PASSWORD,
      },
    })
      .then((res) => {
        // add the token to all api calls
        axios.defaults.headers.common[
          'Authorization'
        ] = `Bearer ${res.data.access_token}`;
        // now we will store the token and the expiration time in the local storage
        const now = new Date();
        const expirationTime = new Date(
          now.getTime() + res.data.expires_in * 1000
        ); // in milliseconds
        localStorage.setItem('token', res.data.access_token);
        localStorage.setItem('expirationTime', expirationTime);
        localStorage.setItem('expiresIn', res.data.expires_in);
        // we will not store the token to the store now but we will add it after we get the user uid from the service below
        return axios.get(restEndpoints.ACCOUNTS_CURRENT);
      })
      .then((res) => {
        commit('AUTH_USER', {
          token: localStorage.getItem('token'),
          uid: res.data.user.id,
          aid: res.data.id,
          roles: res.data.roles,
          userName: `${res.data.user.firstName} ${res.data.user.lastName}`,
          photo: res.data.user.photo,
        });
        // user id
        localStorage.setItem('uid', res.data.user.id);
        // account id
        localStorage.setItem('aid', res.data.id);
        // roles array
        localStorage.setItem('roles', res.data.roles);
        // user name
        localStorage.setItem(
          'userName',
          `${res.data.user.firstName} ${res.data.user.lastName}`
        );
        // user photo url
        localStorage.setItem('photo', res.data.user.photo);
      })
      .then(() => {
        // sign out automatically after the token expiration
        dispatch(
          'SIGN_OUT_AFTER_EXPIRATION',
          parseInt(localStorage.getItem('expiresIn')) || 0
        );
      });
  },
  AUTO_SIGN_IN({ commit }) {
    // in this action we will check if we already have a token and if it is not expired -> we will login the user automatically
    // this action will be called at the application start , in the App component
    const token = localStorage.getItem('token');
    if (!token) return;
    const expirationTime = new Date(localStorage.getItem('expirationTime')); // everything in the local storage is stored as string
    if (!expirationTime) return;
    const now = new Date();
    if (now >= expirationTime) return;
    // the token is still valid, add the token to all api calls
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

    // process data
    const uid = localStorage.getItem('uid');
    if (!uid) return;
    const aid = localStorage.getItem('aid');
    if (!aid) return;
    const roles = localStorage.getItem('roles');
    if (!roles) return;
    const userName = localStorage.getItem('userName');
    if (!userName) return;
    const photo = localStorage.getItem('photo');
    if (!photo) return;

    commit('AUTH_USER', { uid, aid, token, roles, userName, photo });
    // redirection rules
    if (roles.includes('ROLE_TRAININGS_READER'))
      router.push({ name: routesNames.HOME_PAGE });
    else if (roles.includes('ROLE_ADMIN'))
      router.push({ name: routesNames.ADMIN_REQUESTS_PAGE });
    // we push here to let the user return to the last route
    else router.push({ name: routesNames.ABOUT_PAGE });
    // if the user has the trainings-reader role we can load the home to show trainings otherwise he will be redirected to the about component
  },
  SIGN_OUT({ commit }) {
    commit('CLEAR_USER_DATA');
    localStorage.removeItem('token');
    localStorage.removeItem('uid');
    localStorage.removeItem('aid');
    localStorage.removeItem('roles');
    localStorage.removeItem('userName');
    localStorage.removeItem('photo');
    localStorage.removeItem('expirationTime');
    localStorage.removeItem('expiresIn');
    router.replace({ name: routesNames.SIGN_IN_PAGE });
    // replace will replace the current route so we can't go back
  },
  SIGN_OUT_AFTER_EXPIRATION({ commit }, expirationTime) {
    // this action will automatically sign out the user after the token expiration
    setTimeout(() => {
      commit('CLEAR_USER_DATA');
      router.replace({ name: routesNames.SIGN_IN_PAGE });
    }, expirationTime * 1000); // expirationTime is in seconds, we should transform it to milliseconds
  },
  // set the user name if changed from the app
  SET_USER_NAME({ commit }, payload) {
    commit('SET_USER_NAME', payload);
  },
  // set the user photo
  SET_USER_PHOTO({ commit }, payload) {
    commit('SET_USER_PHOTO', payload);
  },
};

const getters = {
  getToken(state) {
    return state.token;
  },
  isAuthenticated(state) {
    return state.token !== null;
  },
  getUid(state) {
    return state.uid;
  },
  getAccountId(state) {
    return state.aid;
  },
  getRoles(state) {
    return state.roles;
  },
  getUserName(state) {
    return state.userName;
  },
  getUserPhoto(state) {
    return state.photo;
  },
  isAdmin(state) {
    // returns true if the current user is an administrator
    return state.roles && state.roles.includes('ROLE_ADMIN');
  },
  hasReadTrainingsPerm(state) {
    // returns true if the current user can read trainings permission
    return state.roles && state.roles.includes('ROLE_TRAININGS_READER');
  },
  hasWriteTrainingsPerm(state) {
    // returns true if the current user can create/modify trainings permission
    return state.roles && state.roles.includes('ROLE_TRAININGS_WRITER');
  },
  hasReadUsersPerm(state) {
    return state.roles && state.roles.includes('ROLE_USERS_READER');
  },
  hasWriteUsersPerm(state) {
    // permission to change email, password, delete account
    return state.roles && state.roles.includes('ROLE_USERS_WRITER');
  },
  canManageTrainings(state) {
    // user can manage trainings if he has these permissions
    return (
      state.roles &&
      state.roles.includes('ROLE_TRAININGS_READER') &&
      state.roles.includes('ROLE_TRAININGS_WRITER') &&
      state.roles.includes('ROLE_USERS_READER')
    );
  },
  canReadTrainings(state) {
    return (
      state.roles &&
      (state.roles.includes('ROLE_ADMIN') ||
        (state.roles.includes('ROLE_TRAININGS_READER') &&
          state.roles.includes('ROLE_USERS_READER')))
    );
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
