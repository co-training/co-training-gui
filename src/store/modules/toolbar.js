import axios from 'axios';
import * as endpoints from '@/constants/rest-endpoints';
import store from '@/store';

/*
 *  this is the toolbar store which holds the state of two elements : the whishlist badge and the user resource data
 *  the whishlist store reflect the little badge in the top of the little heart in the toolbar; each time we add or remove a training to the whishlist we will update the badge
 *  the resource store reflect the user resources which are the user's budget and the available number of days; each time we participate or leave a training we will update these data
 *  this store will be initialized in the Home component
 */
const state = {
  whishlist: 0,
  resource: {},
};

const mutations = {
  INIT_WHISHLIST(state, payload) {
    state.whishlist = payload;
  },
  INCREMENT_WHISHLIST(state) {
    state.whishlist++;
  },
  DECREMENT_WHISHLIST(state) {
    state.whishlist--;
  },
  INIT_RESOURCE(state, payload) {
    state.resource = payload;
  },
};

const actions = {
  INIT_WHISHLIST({ commit }) {
    const uid = store.getters['auth/getUid'];
    axios
      .get(endpoints.USER_COUNT_WHISHLIST_TRAININGS.replace('{uid}', uid))
      .then((res) => {
        commit('INIT_WHISHLIST', res.data);
      });
  },
  INCREMENT_WHISHLIST({ commit }) {
    commit('INCREMENT_WHISHLIST');
  },
  DECREMENT_WHISHLIST({ commit }) {
    commit('DECREMENT_WHISHLIST');
  },
  INIT_RESOURCE({ commit }) {
    // each time the user participated or leave a training we will check the server for the remaining resources
    const uid = store.getters['auth/getUid'];
    axios.get(endpoints.USER_RESOURCES.replace('{uid}', uid)).then((res) => {
      commit('INIT_RESOURCE', res.data);
    });
  },
};

const getters = {
  getWhishlist(state) {
    return state.whishlist;
  },
  getResource(state) {
    return state.resource;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
