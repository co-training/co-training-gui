import axios from 'axios';
import * as endpoints from '@/constants/rest-endpoints';

// this store will be used in the training component because it is a complex component src/components/training/
const state = {
  training: {}, // this is the training
  loading: true,
};

const mutations = {
  SET_TRAINING(state, training) {
    state.training = training;
    state.loading = false;
  },
  LOAD(state) {
    state.loading = true;
  },
};

const actions = {
  SET_TRAINING({ commit }, { id }) {
    // sync action
    commit('LOAD');
    // async action
    axios.get(endpoints.TRAINING.replace('{id}', id)).then((res) => {
      commit('SET_TRAINING', res.data);
    });
  },
};

const getters = {
  getTraining(state) {
    return state.training;
  },
  getLoading(state) {
    return state.loading;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
