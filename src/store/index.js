import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/authentication';
import training from './modules/training';
import toolbar from './modules/toolbar';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    training,
    toolbar,
  },
});
