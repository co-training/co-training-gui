import axios from 'axios';
import * as endpoints from '@/constants/rest-endpoints';
import * as alerts from '@/utils/alerts-utils';

export const authorStatsMixin = {
  data() {
    return {
      summary: [
        { name: 'trainings', title: 'Trainings', value: null },
        { name: 'participants', title: 'Participants', value: null },
        { name: 'students', title: 'Students', value: null },
        { name: 'reviews', title: 'Reviews', value: null },
        { name: 'rating', title: 'Rating', value: null },
      ],
    };
  },
  mounted() {
    axios
      .get(endpoints.USER_EVALUATION.replace('{uid}', this.uid))
      .then((res) => {
        this.summary[0].value = res.data.trainings.toString();
        this.summary[1].value = res.data.participants.toString();
        this.summary[2].value = res.data.students.toString();
        this.summary[3].value = res.data.reviews.toString();
        this.summary[4].value = res.data.rating.toString();
      })
      .catch((err) => {
        this.snackbar = alerts.error(err);
      });
  },
};
