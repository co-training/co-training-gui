// This mixin contains methods and api calls to participate to a training and to leave it

import axios from 'axios';
import * as endpoints from '@/constants/rest-endpoints';
import * as alerts from '@/utils/alerts-utils';

export const participateMixin = {
  methods: {
    participate() {
      this.progress = true;
      axios
        .get(
          endpoints.USER_FOLLOWED_TRAININGS_PARTICIPATE.replace(
            '{uid}',
            this.getUid()
          ).replace('{id}', this.getTrainingId())
        )
        .then(() => {
          this.participated = true;
          this.snackbar = alerts.info(
            'You have been successfully added to the list of participants'
          );
          // update the toolbar store
          this.$store.dispatch('toolbar/INIT_RESOURCE');
          // this method is to declare in the component it will be called just after the call so you can refresh the remaining places or whatever
          this.refresh();
        })
        .catch((err) => {
          this.snackbar = alerts.error(err);
        })
        .then(() => {
          this.progress = false;
        });
    },
    leave() {
      this.progress = true;
      axios
        .get(
          endpoints.USER_FOLLOWED_TRAININGS_LEAVE.replace(
            '{uid}',
            this.getUid()
          ).replace('{id}', this.getTrainingId())
        )
        .then(() => {
          this.participated = false;
          this.snackbar = alerts.info(
            'You have been successfully removed from the list of participants'
          );
          // update the toolbar store
          this.$store.dispatch('toolbar/INIT_RESOURCE');
          // this method is to declare in the component it will be called just after the call so you can refresh the remaining places or whatever
          this.refresh();
        })
        .catch((err) => {
          this.snackbar = alerts.error(err);
        })
        .then(() => {
          this.progress = false;
        });
    },
    getUid() {
      return this.$store.getters['auth/getUid'];
    },
  },
};
