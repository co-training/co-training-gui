// This mixin contains methods and api calls to add and remove a training from the whishlist

import axios from 'axios';
import * as endpoints from '@/constants/rest-endpoints';
import * as alerts from '@/utils/alerts-utils';

export const whishlistMixin = {
  methods: {
    add() {
      this.progress = true;
      axios
        .get(
          endpoints.USER_WHISHLIST_TRAININGS_ADD.replace(
            '{uid}',
            this.getUid()
          ).replace('{id}', this.getTrainingId())
        )
        .then(() => {
          this.inWhishlist = true;
          this.snackbar = alerts.info(
            'The training has been successfully added to your whishlist'
          );
          // increment the toolbar whishlist store
          this.$store.dispatch('toolbar/INCREMENT_WHISHLIST');
        })
        .catch((err) => {
          this.snackbar = alerts.error(err);
        })
        .then(() => {
          this.progress = false;
        });
    },
    remove() {
      this.progress = true;
      axios
        .get(
          endpoints.USER_WHISHLIST_TRAININGS_REMOVE.replace(
            '{uid}',
            this.getUid()
          ).replace('{id}', this.getTrainingId())
        )
        .then((res) => {
          this.inWhishlist = false;
          this.snackbar = alerts.info(
            'The training has been successfully removed from your whishlist'
          );
          // decrement the toolbar whishlist store
          this.$store.dispatch('toolbar/DECREMENT_WHISHLIST');
        })
        .catch((err) => {
          this.snackbar = alerts.error(err);
        })
        .then(() => {
          this.progress = false;
        });
    },
    getUid() {
      return this.$store.getters['auth/getUid'];
    },
  },
};
