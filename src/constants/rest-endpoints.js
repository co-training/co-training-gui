/*
 * URL to get the authentication token
 */
export const OAUTH_URL = process.env.VUE_APP_OAUTH_URL;
export const OAUTH_CLIENT_ID = 'client-web';
export const OAUTH_CLIENT_PASSWORD = 'change_me';
export const OAUTH_GRANT_TYPE = 'password';

/*
 * URLs of application rest api
 */
export const APP_URL = process.env.VUE_APP_APIS_URL;

// Training api
export const TRAININGS = '/trainings';
export const TRAININGS_COUNT = '/trainings/count';
export const TRAINING = '/trainings/{id}';
export const TRAINING_EVALUATION = '/trainings/{id}/evaluate';
export const TRAINING_PARTICIPANTS = '/trainings/{id}/participants';
export const TRAINING_COUNT_PARTICIPANTS = '/trainings/{id}/participants/count';
export const TRAINING_COUNT_REVIEWS = '/trainings/{id}/reviews/count';

// Reviews api
export const REVIEWS = '/reviews';
export const REVIEWS_SEARCH = '/reviews/search';
export const REVIEWS_UPDATE = '/reviews/{id}';

// Teams api
export const TEAMS = '/teams';
export const TEAM = '/teams/{id}';

// Accounts api
export const ACCOUNTS = '/accounts';
export const ACCOUNT = '/accounts/{id}';
export const ACCOUNTS_COUNT = '/accounts/count';
export const ACCOUNTS_NEW = '/accounts/new';
export const ACCOUNTS_CURRENT = '/accounts/current';
export const ACCOUNTS_DISABLE_ALERT = '/accounts/{id}/off';
export const ACCOUNTS_ENABLE = '/accounts/{id}/enable';
export const ACCOUNTS_DISABLE = '/accounts/{id}/disable';
export const ACCOUNTS_ROLES = '/accounts/roles';
export const ACCOUNTS_ROLES_UPDATE = '/accounts/{id}/roles';
export const ACCOUNTS_UPDATE_PASSWORD = '/accounts/{id}/password';
export const ACCOUNTS_UPDATE_EMAIL = '/accounts/{id}/email';

// Levels api
export const LEVELS = '/levels';

// Tags api
export const TAGS = '/tags/search';

// User api
export const USERS = '/users';
export const USER = '/users/{uid}';
export const USER_DESCRIPTION = '/users/{uid}/describe';
export const USER_GIVEN_TRAININGS = '/users/{uid}/trainings/given';
export const USER_RESOURCES = '/users/{uid}/resources';
export const USER_TEAM = '/users/{uid}/team';

// training participation
export const USER_FOLLOWED_TRAININGS = '/users/{uid}/trainings/followed';
export const USER_FOLLOWED_TRAININGS_PARTICIPATE =
  '/users/{uid}/trainings/followed/{id}/participate';
export const USER_FOLLOWED_TRAININGS_LEAVE =
  '/users/{uid}/trainings/followed/{id}/leave';

// whishlist
export const USER_WHISHLIST_TRAININGS = '/users/{uid}/whishlist';
export const USER_COUNT_WHISHLIST_TRAININGS = '/users/{uid}/whishlist/count';
export const USER_WHISHLIST_TRAININGS_ADD = '/users/{uid}/whishlist/{id}/add';
export const USER_WHISHLIST_TRAININGS_REMOVE =
  '/users/{uid}/whishlist/{id}/remove';

// stats
export const USER_EVALUATION = '/users/{uid}/evaluate';
