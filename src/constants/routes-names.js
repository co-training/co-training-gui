// In this file we will define our routes names

// Home route
export const HOME_PAGE = 'HOME_PAGE';

// All Trainings route
export const TRAINING_PAGE = 'TRAINING_PAGE';

// My Trainings routes
export const MY_TRAININGS_PAGE = 'MY_TRAININGS_PAGE';
export const CREATE_NEW_TRAINING_PAGE = 'CREATE_NEW_TRAINING_PAGE';
export const MANAGE_TRAINING_PAGE = 'MANAGE_NEW_TRAINING_PAGE';
export const PREVIEW_TRAINING_PAGE = 'PREVIEW_NEW_TRAINING_PAGE';
export const EDIT_TRAINING_PAGE = 'EDIT_NEW_TRAINING_PAGE';
export const DELETE_TRAINING_PAGE = 'DELETE_NEW_TRAINING_PAGE';

// Admin Panel routes
export const ADMIN_PAGE = 'ADMIN_PAGE';
export const ADMIN_USERS_PAGE = 'ADMIN_USERS_PAGE';
export const ADMIN_REQUESTS_PAGE = 'ADMIN_REQUESTS_PAGE';
export const ADMIN_TEAMS_PAGE = 'ADMIN_TEAMS_PAGE';

// About the application route
export const AUTHOR_PAGE = 'AUTHOR_PAGE';

// About the application route
export const ABOUT_PAGE = 'ABOUT_PAGE';

// User resources route
export const RESOURCES_PAGE = 'RESOURCES_PAGE';

// Header routes
export const WHISH_LIST_PAGE = 'WHISH_LIST_PAGE';
export const SETTINGS_PAGE = 'SETTINGS_PAGE';
export const PROFILE_PAGE = 'PROFILE_PAGE';

// Sign-in and Sign-up pages
export const SIGN_IN_PAGE = 'SIGN_IN_PAGE';
export const SIGN_UP_PAGE = 'SIGN_UP_PAGE';

// Logout route
export const LOG_OUT_ROUTE = 'LOG_OUT_ROUTE';
