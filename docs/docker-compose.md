# Docker Compose configuration

In this file we will explain the steps that i've followed to create a docker compose environment for the frontend app. It's the best way to run the application and test it quickly without configuring anything.

If you don't know what's a docker compose, you can check the docker documentation :

- https://docs.docker.com/compose/

## Yaml file

Here's our `docker-compose.yaml` definition :

```yaml
version: '3'

services:
  co-training-rest:
    container_name: co-training-rest-app
    image: mouhamedali/co-training-backend
    environment:
      - SPRING_PROFILES_ACTIVE=h2
      - SERVER_PORT=4000
    ports:
      - 4444:4000

  co-training-gui:
    container_name: co-training-gui-app
    image: mouhamedali/co-training-gui:0.0.1-DEV-SNAPSHOT
    build:
      context: .
      dockerfile: Dockerfile.dev
      args:
        OAUTH_URL: http://localhost:4444/co-training/oauth/token
        APIS_URL: http://localhost:4444/co-training/v1
    environment:
      - NODE_ENV=development
    ports:
      - 5000:80
```

As you can see we are using the compose version 3.

### co-training-rest service

This service will run our backend application. It takes the latest docker image of the backend and run it using the `h2` profile which uses an in memory data-source so there is no need to install a database. The container runs on the port 4000 and you can reach it from you localhost using the port 4444.

### co-training-gui service

In this service, we will run the frontend application after building it. The built image is `mouhamedali/co-training-gui:0.0.1-DEV-SNAPSHOT` and the docker file used in the build is `Dockerfile.dev`. In the next section we will talk about this docker file.

As our frontend app communicate with the backend, we've to find a way to externalize this backend url so our frontend source code will not change if we change the backend server. The best way to do that is to use environment variables.
If you check the `src/constants/rest-endpoints.js` you'll find the backend server config :

```js
/*
 * URL to get the authentication token
 */
export const OAUTH_URL = process.env.VUE_APP_OAUTH_URL;

/*
 * URLs of application rest api
 */
export const APP_URL = process.env.VUE_APP_APIS_URL;
```

And these environment variables can be retrieved from the `.env` file in the root dir of the project. This is the case when you run the app using `npm run serve` but in our case (docker compose) we will pass this configuration as args.

Arguments are environment variables accessible only during the build process. Check the docker documentation :

- https://docs.docker.com/compose/compose-file/compose-file-v3/#args

and these links will helps you to understand how to use environment variables in vuejs :

- [Vuejs Environment Variables](https://cli.vuejs.org/guide/mode-and-env.html#environment-variables)

- [Using Environment Variables with Vue.js](https://stackoverflow.com/questions/50828904/using-environment-variables-with-vue-js)

So the args part of this docker-compose file will be used in the Docker file later.

Last thing to add is the `NODE_ENV=development` environment variable. This variable is mandatory because of the `vue.config.js` config file. In this file, we are declaring the root context of our application if we perform a build to the production but for development we keep the default root path.

```js
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/frontend' : '/',
  transpileDependencies: ['vuetify'],
};
```

## Dev Dockerfile

The docker file used in the docker compose is not the same as the one used to build the delivery. The docker file is as below :

```dockerfile
FROM node:15.5.1-alpine3.10 as ui-builder

# define workdir
WORKDIR /app

# install dependencies
COPY package*.json ./
RUN npm install -g @vue/cli
RUN npm install

# copy sources
COPY . .

# remove dist folder if exists
RUN rm -rf dist

# urls configs
ARG OAUTH_URL
ENV VUE_APP_OAUTH_URL $OAUTH_URL
ARG APIS_URL
ENV VUE_APP_APIS_URL $APIS_URL

# build the dist
RUN ./node_modules/.bin/vue-cli-service build --mode development

# build the final image
FROM nginx:1.18.0-alpine
COPY  --from=ui-builder /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

We are using a multi-stage builds here. In the first stage, we are building the artifact and in the second we serve that artifact using NGINX.

Have a look at this link to know more :

- [Dockeriser une app Vue.js](https://fr.vuejs.org/v2/cookbook/dockerize-vuejs-app.html)

The arguments `OAUTH_URL` and `APIS_URL` are retrieved from the `docker-compose.yml` file and they are used to set respectively the env variables `VUE_APP_OAUTH_URL` and `VUE_APP_APIS_URL` which are used by the app to connect to the authentication and api servers of the platform. Check the example here :

- [Passing API URL environment variable](https://shekhargulati.com/2019/01/18/dockerizing-a-vue-js-application/)

If you build the app using `npm run build`, the production mode will be used by default. that's why we are using the `./node_modules/.bin/vue-cli-service build --mode development` command to switch to development mode.

check more here :

- [Modes and Environment Variables](https://cli.vuejs.org/guide/mode-and-env.html#modes)

- [Configure Vue to have a development and a production build](https://stackoverflow.com/questions/55409729/configure-vue-to-have-a-development-and-a-production-build)
