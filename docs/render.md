# Render hosting

Render is a unified cloud to build and run all your apps and websites with free SSL, global CDN, private networks and auto deploys from Git and Gitlab.

- https://render.com/

You can host your static site for free on render and it makes it very easy if you have a github or a gitlab repo. This link contains more details :

- https://render.com/docs/deploy-vue-js

## Configure the build

As explained in the project, our app will use an api exposed by the backend and to configure this backend urls we have to pass them as environment variables. To achieve that you have to go to the `Environment` are of your project :

![Selection_409](/uploads/0240ba3a72e3ad6f94785d29e31266cb/Selection_409.png)

and configure urls. In my case, i'm using this config :

```shell
NODE_ENV = stage
VUE_APP_OAUTH_URL = https://co-training-backend.herokuapp.com/co-training/oauth/token
VUE_APP_APIS_URL = https://co-training-backend.herokuapp.com/co-training/v1
```

Why stage ?

Well because this app has been created to be hosted on kubernetes and to run on different **context path** then the root **/**. As mentioned `vue.config.js` file.

![Selection_411](/uploads/66db0e5b5039ebc2d05f4bf619a43fc5/Selection_411.png)

As we will use render and not kubernetes, there's not need for the **/frontend** in the path.

## Redeploy the app

Go to `Manual Deploy` and click on `Clear build cache & deploy`.

![Selection_410](/uploads/6ebf1b2e4ff5ae017592ceaa6c52de4a/Selection_410.png)

## Client side routing

Routing is handled by **Vue** as this is single page application and not the server. That's why we need to redirect all the requests to the `index.html` (which contains the script to handle routing) and prevent the server from routing our requests.

Go `Redirects/Rewrites` and add this configuration :

![Selection_412](/uploads/6fcebeee8313aba3fd5c9863e4c171b3/Selection_412.png)

You'll find more details here :

- https://render.com/docs/deploy-vue-js#using-client-side-routing
