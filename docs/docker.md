# Dockerfile of the application

In this section we will talk about the dockerfile used to build the application and run it on the production platform.

## Dockerfile

Let's start with the dockerfile content :

```dockerfile
FROM nginx:1.18.0-alpine

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# copy distribution files
COPY ./dist /usr/share/nginx/html

# copy the nginx conf
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

# exopse the app on the port 80
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
```

This is a simple configuration to build a docker image of a vuejs application. Before building the image, we have to build first the vuejs distribution using :

```shell
$ npm run build
```

The generated distribution will be copied to nginx root folder `/usr/share/nginx/html`. Then we copy the `nginx.conf` located under the `nginx` folder. This file contains the configuration of our server. We will talk about this file later. Finally, we are exposing the app on the port 80 and defining the script to run after setting up the container.

Now, we can build the image and run the app :

```shell
$ docker build -t co-training-gui:v0.0.0 .

$ docker container run --rm --name co-training-gui -p 8080:80 co-training-gui:v0.0.0
```

## Nginx config

The config of our server is defined under `nginx/nginx.conf`. Let's have a look at this file :

```
# auto detects a good number of processes to run
worker_processes auto;

#Provides the configuration file context in which the directives that affect connection processing are specified.
events {
    # Sets the maximum number of simultaneous connections that can be opened by a worker process.
    worker_connections 1024;
    # Tells the worker to accept multiple connections at a time
    multi_accept on;
}


http {
    # We should include this directive in the nginx.conf file otherwise all the styles are rendered as plain text in the browser.
    include       /etc/nginx/mime.types;
    # what is the default one
    default_type  application/octet-stream;

    # Sets the path, format, and configuration for a buffered log write
    log_format compression '$remote_addr - $remote_user [$time_local] '
        '"$request" $status $upstream_addr '
        '"$http_referer" "$http_user_agent"';

    server {
        # listen on port 80
        listen 80;
        # save logs here
        access_log /var/log/nginx/access.log compression;

        # where the root here
        root /usr/share/nginx/html;
        # what file to server as index
        index index.html index.htm;

        # rewrite any url that starts with frontend
        rewrite ^/frontend(.*)$ $1 last;

        location = /healthz {
            access_log off;
            # health check endpoint : to add if you want to add a /health endpoint
            return 200 '{"status":"up"}';
            add_header Content-Type text/json;
        }

        location / {
            # First attempt to serve request as file, then
            # as directory, then fall back to redirecting to index.html
            try_files $uri $uri/ /index.html;
        }

        # Media: images, icons, video, audio, HTC
        location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc)$ {
          expires 1M;
          access_log off;
          add_header Cache-Control "public";
        }

        # Javascript and CSS files
        location ~* \.(?:css|js)$ {
            try_files $uri =404;
            expires 1y;
            access_log off;
            add_header Cache-Control "public";
        }

        # Any route containing a file extension (e.g. /devicesfile.js)
        location ~ ^.+\..+$ {
            try_files $uri =404;
        }
    }
}

```

The majority of the commands are explained in the comments but i'll talk further about some the features.

### rewrite

```
rewrite ^/frontend(.*)$ $1 last;
```

The rewrite config is used to , as its name says, to replace a part of the received url and in our case `/frontend` with nothing. But why are we using this config ?

For the production delivery we have to change the root context of our application. We cannot simply use the default one, the `/`. As we are deploying our application on ingress and that one deploys the app under `/frontend`, we have to make our app work on this path and not the default one.

That's not clear, the ingress config defined in `k8s/quality/ingress.yaml` uses already the rewrite feature ?

Well, yes. If you remove the rewrite from this file, it works fine but only on the cloud. I added this feature so we can run the app on the local machine using a different root path.
If you keep this file without rewrite config, the server will try to search for files under the `frontend` directory and as this directory does not exist, the server returns a 404 error.

### /healthz

This url path will be used by the kubernetes to check if our application is available or not. It is used in the liveness and readiness probes in `k8s/quality/deployment.yaml`.
