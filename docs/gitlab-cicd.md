# Gitlab Pipeline

In this section, I will explain the followed steps to create the ci/cd pipeline of the project using the gitlab jobs.
We will use the GitLab CI/CD to automatically build, test and deploy the application.
GitLab CI/CD uses a file in the root of the repository, named `.gitlab-ci.yml`, to read the definitions for jobs that will be executed by the configured runners.
This doc is a part of the backend ci/cd documentation which you can find here :

- https://gitlab.com/co-training/co-training-backend/-/blob/master/docs/gitlab-cicd.md#gitlab-pipeline

So you can start with it before reading this one and if something is not clear or not well explained you'll the explanation in the backend doc.

I will start with the global gitlab environment variables. As you've notice it, the structure of my gitlab project is like this :

```
co-training
│
└─── co-training-gui
│
└─── co-training-backend
│
└─── co-training-deployment
```

Environment variables in the `co-training` are inherited by other projects. So env variables of the main project are :

![1_env-global](/uploads/369448c7e670e52c28541661b7a493d6/1_env-global.png)

check the backend documentation to have more details about these variables.

Env variables of this project are :

![env_variables](/uploads/57472633799da322e7028348e196aa18/env_variables.png)

Here we have two configurations, the first is for the STAGE platform and the second is for PRODUCTION platform.

So these variables are used in the frontend build so the app can make api calls to the server.
For the stage we will store the server urls into a file and then when building the app we will use it as a `.env` file. For production, we will use simple environment variables.
I've used the two configurations to present the gitlab features but i prefer the second way.

Here's the content of variables :

![env_variables_1](/uploads/5b79cfe9c920e29b7490e6382b2a1462/env_variables_1.png)

```
VUE_APP_OAUTH_URL = http://34.77.177.135/backend/co-training/oauth/token
VUE_APP_APIS_URL = http://34.77.177.135/backend/co-training/v1
```

![env_variables_2](/uploads/d1b6697a4bee763becee9d407a8a254a/env_variables_2.png)

PROD_API_URLS : `http://35.241.245.0/backend/co-training/v1`

![env_variables_3](/uploads/86f22f4c424e23fa383f2786479ebabd/env_variables_3.png)

PROD_OAUTH_URL : `http://35.241.245.0/backend/co-training/oauth/token`

As you guess `34.77.177.135` is the STAGE backend server and `35.241.245.0` is the PRODUCTION. After deploying the backend you can get this information from the `google cloud console`.

![3_ingress_backend_url](/uploads/bf7a2a2416f6deac5c9351f4ab4c67b6/3_ingress_backend_url.png)

Protected branches :

Go to Settings -> Repository -> Protected Branches and make these branches protected so you can use the env variables.

![protected_branches](/uploads/daf1ece223f1c98fb09b3fd609acd860/protected_branches.png)

## The pipeline of the co-training-gui

As for the backend, in the frontend we will use kubernetes technology. Here's the pipeline :

![gitlab-cicd-workflow-frontend](/uploads/77f5390f9e448f72b6ff28e9933b11b5/gitlab-cicd-workflow-frontend.jpg)

This doc does not contain all the details, so check the backend docs to know more :

- https://gitlab.com/co-training/co-training-backend/-/blob/master/docs/gitlab-cicd.md#gitlab-pipeline

### pipeline default image

As our application is a vuejs app, we will use `node:15.5.1-alpine3.10` as docker hub image in the pipeline but you can use another image.

### pipeline stages

```yaml
stages:
  - install
  - test
  - build
  - publish
  - deploy
```

The pipeline stages are :

- install : in this stage we will download the project dependencies
- test : in this stage we run the unit tests
- build : this stage is used to build the project distribution
- publish : in this stage we will create a docker image of the image and push it to dockerhub
- deploy : this stage is used to deploy the app to gke

### dependencies job

```yaml
dependencies:
  stage: install
  script:
    - npm install -g @vue/cli
    - npm install
```

This job will bo executed on all branches after a push or a merge.
In this job we will install the vue cli and then run `npm install` which will install the project dependencies and cache them so they can be used in the next jobs.

### test job

```yaml
test:
  stage: test
  script:
    - npm run test:unit
```

This job will be also executed all the time. It will run the unit tests of the project.

### build:quality job

```yaml
build:quality:
  stage: build
  variables:
    CI_DEBUG_TRACE: 'false'
  before_script:
    # retrieve the new version from the branch name (branch on which we are doing the merge)
    - version="$(echo $CI_COMMIT_REF_NAME | cut -d '/' -f 2)"-STAGE
    - echo "The new version to release is $version"
    # set the backend urls
    - rm -f .env
    - touch .env
    - cat $BACKEND_URLS_STAGE_FILE > .env
  script:
    - npm run build
    # write the version to the build.env so it can be used in the next jobs
    - echo "BUILD_VERSION=$version" >> build.env
  artifacts:
    when: on_success
    paths:
      - dist
    reports:
      dotenv: build.env
  only:
    - /^release\/\d+\.\d+\.\d+$/
```

This job will be triggered after merging the develop branch to a release branch.

#### before script

Like in the backend, in the before script we will get the release version and store it in the `version` variable. As this is a test version, it will be tagged with `-STAGE` at the end.

Before building the distribution, we have to mention the backend server url. To do this, we will remove the `.env` already exists in the project which contains the development configuration and replace it with the `$BACKEND_URLS_STAGE_FILE` which contains the STAGE server backend url.

#### script

In the script area, we will build the project delivery and make it an artifact. finally, we will store the version in the `build.env` so it can be used in next jobs.

### publish:quality job

```yaml
publish:quality:
  stage: publish
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  variables:
    CI_REGISTRY_IMAGE: 'mouhamedali/co-training-gui'
    CI_DEBUG_TRACE: 'false'
  before_script:
    - echo " The build version is $BUILD_VERSION"
    - docker login --username mouhamedali --password-stdin < $GITLAB_DOCKER_HUB_TOKEN
  script:
    # Build the image, pull the latest image so it can behave as a docker cache
    - docker pull $CI_REGISTRY_IMAGE || true
    - echo "Building the docker image $CI_REGISTRY_IMAGE:$BUILD_VERSION"
    - docker build --cache-from "$CI_REGISTRY_IMAGE" -t "$CI_REGISTRY_IMAGE:$BUILD_VERSION" .
    # Push the new image
    - docker push $CI_REGISTRY_IMAGE:$BUILD_VERSION
  dependencies:
    # get artifacts only from the build job
    - build:quality
  only:
    - /^release\/\d+\.\d+\.\d+$/
```

This job will be triggered just after the `build:quality` job finishes. Its main purpose is to create a docker image of the application. It will also tag the image with the version that matches the release branch version.
Please , to have more details, check the backend docs, as this section has been already explained.

#### before script

From the last job, we will get the release version and the artifact. So in this section we are logging the version and then connecting to the my docker hub account.
Connection parameters are retrieved from the gitlab global variables.

#### script

At first we will pull the latest docker image of our app. This image will behave as a cache in the build of the release docker image. The next step is to build and push the release docker image.

### deploy:quality job

```yaml
deploy:quality:
  stage: deploy
  image: 'google/cloud-sdk:alpine'
  cache: {}
  before_script:
    - echo " The build version is $BUILD_VERSION"
    # Install envsubst
    - apk update && apk upgrade && apk add gettext
    # Install kubectl
    - gcloud components install kubectl
  script:
    # set the project config
    - gcloud auth activate-service-account --key-file=$GITLAB_GC_SERVICE_KEY
    # connect to the cluster
    - gcloud container clusters get-credentials quality-cluster --zone europe-west1-d --project co-training-293418
    # show configuration
    - kubectl config current-context
    # parse the deployment file and replace the $BUILD_VERSION with the current version, mktemp creates a temp file
    - tmp=$(mktemp)
    # envsubst is used to substitute environment variable placeholders inside configuration files
    - envsubst < k8s/quality/deployment.yaml  > "$tmp" &&  mv "$tmp" k8s/quality/deployment.yaml
    - cat k8s/quality/deployment.yaml
    # deploy
    - kubectl apply -f k8s/quality
  dependencies:
    - build:quality
  only:
    - /^release\/\d+\.\d+\.\d+$/
```

This job will be triggered just after the `publish:quality` job finishes. Its main purpose is to deploy the last created docker image to quality platform so the test team can make tests and validate or not the deployment to the production environment.

Please , to have more details, check the backend docs :

- https://gitlab.com/co-training/co-training-backend/-/blob/master/docs/gitlab-cicd.md#deployquality-job

PN : you have to install the backend project on the quality platform before running this job.

### build:production job

```yaml
# create the build and tag the version
build:production:
  stage: build
  before_script:
    # set the backend urls
    - export VUE_APP_APIS_URL="$(echo $PROD_API_URLS)"
    - export VUE_APP_OAUTH_URL="$(echo $PROD_OAUTH_URL)"
    # install git on this job as we will change the pom version
    - apk update && apk add git
    # configure the git repo ( we will change the project version and push the commit ) with the gitlab personal token
    - git remote set-url origin https://$GITLAB_USER_LOGIN:$GITLAB_CICD_ACCESS_TOKEN@gitlab.com/$CI_PROJECT_PATH.git
    # configure the git parameters like user.name and user.email
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    # build the app using the production urls
    - npm run build
    - git fetch
    # get the version from the git as gitlab does not provide this data https://stackoverflow.com/questions/59877180/gitlab-ci-get-source-branch-after-merge-request-has-been-accepted
    - version="$(git branch -r | awk '/release/{print $0}' | cut -d '/' -f 3 | sort -n | tail -1)"
    - echo "The new version to release is $version"
    # Tag the release and push it to the repository
    - git tag -a "v$version" -m "Create release version $version"
    - git push origin "v$version"
    # write the version to the build.env so it can be used in the next jobs
    - echo "BUILD_VERSION=$version" >> build.env
  artifacts:
    when: on_success
    paths:
      - dist
    reports:
      dotenv: build.env
  only:
    - master
```

This job will be triggered just after the merging a release branch into the master branch. The main purpose of this job is to build the distribution for the production platform.

#### before script

The first step to do in this section is to configure the `vuejs` environment variables so it can be used in the build process. Variables are already held by `PROD_API_URLS` and `PROD_OAUTH_URL`. In this job we are exporting these variables but with new names understandable by the `vuejs` app which are `VUE_APP_APIS_URL` and `VUE_APP_OAUTH_URL`. The `export` is mandatory here otherwise the `npm` process cannot access these variables.

Then, we will setup the git configuration so we can get the release version and tag the repository. To have more details about the git config, check this link :

- https://gitlab.com/co-training/co-training-backend/-/blob/master/docs/gitlab-cicd.md#buildjar-snapshot-job

#### script

After configuring the backend urls, we can run the build process of the app.

As we are not in a release branch (because this is the master), gitlab does not provide any variable so we can't get the release version (normally after merging a release to a master branch, we should get the source branch name but this is not the case).
The workaround of this issue, is to get the release branch name from git as explained in this link :

- https://stackoverflow.com/questions/59877180/gitlab-ci-get-source-branch-after-merge-request-has-been-accepted

Well this solution was supposed to work but it doesn't for me. That's why i used another solution which is `get the release version from the last release branch name`.

```yaml
- git fetch
- version="$(git branch -r | awk '/release/{print $0}' | cut -d '/' -f 3 | sort -n | tail -1)"
- echo "The new version to release is $version"
```

We have to fetch the last updates on the repo so we can have access to last git objects modifications. Gitlab doesn't do it by default so this line is important. Then, we can get all the remote branches. We have to filter on release branches (with awk) and get only the last part which hold the release version (with cut). The last thing to do is to sort the results (asc order) and get the last number (with tail).

After getting the release version number, we will create a tag with the release version and push it to the project repo. Finally, we will store it in the `build.end` so it can be available in next jobs.

### publish:production job

```yaml
# Build the app docker latest image and push it to the docker-hub registry
publish:production:
  stage: publish
  image: docker:19.03.12
  services:
    # this image will behave as a docker daemon
    - docker:19.03.12-dind
  variables:
    CI_REGISTRY_IMAGE: 'mouhamedali/co-training-gui'
  before_script:
    - docker login --username mouhamedali --password-stdin < $GITLAB_DOCKER_HUB_TOKEN
  script:
    # Build the image, pull the latest image so it can behave as a docker cache
    - docker pull $CI_REGISTRY_IMAGE || true
    - echo "Building the docker image $CI_REGISTRY_IMAGE:$BUILD_VERSION"
    - docker build --cache-from "$CI_REGISTRY_IMAGE" -t "$CI_REGISTRY_IMAGE:$BUILD_VERSION" .
    # Push the new image
    - docker push $CI_REGISTRY_IMAGE:$BUILD_VERSION
    # Tag and push the tag
    - docker tag "$CI_REGISTRY_IMAGE:$BUILD_VERSION" "$CI_REGISTRY_IMAGE:latest"
    - docker push "$CI_REGISTRY_IMAGE:latest"
  dependencies:
    # get artifacts only from the build job
    - build:production
  only:
    - master
```

This job will be triggered just after the `build:production` finishes. Its main purpose is to build a docker image to the production platform with the release version.

#### script

At first we will pull the latest docker image of our app. This image will behave as a cache in the build of the release docker image. The next step is to build and push the release docker image to docker hub. Then, tag that image with the `latest` tag so it can be used as a cache the next time we run the job. Finally, push also that latest image.

### deploy:production job

```yaml
deploy:production:
  stage: deploy
  image: 'google/cloud-sdk:alpine'
  when: manual
  cache: {}
  before_script:
    - echo " The version to deploy is $BUILD_VERSION"
    # Install envsubst
    - apk update && apk upgrade && apk add gettext
    # Install kubectl
    - gcloud components install kubectl
  script:
    # set the project config
    - gcloud auth activate-service-account --key-file=$GITLAB_GC_SERVICE_KEY
    # connect to the cluster
    - gcloud container clusters get-credentials production-cluster --zone europe-west1-d --project co-training-293418
    - kubectl config current-context
    # change the tag and deploy it
    - tmp=$(mktemp)
    - envsubst < k8s/production/deployment.yaml  > "$tmp" &&  mv "$tmp" k8s/production/deployment.yaml
    - cat k8s/production/deployment.yaml
    # deploy
    - kubectl apply -f k8s/production
  dependencies:
    - build:production
  only:
    - master
```

This job will be triggered just after the `publish:production` finishes. Its main purpose is to build a docker image to the production platform. The docker image tag will be the release version which has been retrieved from the `build:production` job.

For more details about the process check the `deploy:quality` job. This job is manual which means that you have to start it from the gitlab web ui after the `publish:production` job finishes otherwise the new version will not be available in the production platform.

## References

- [Cache node modules](https://stackoverflow.com/questions/55359804/gitlab-ci-npm-doesnt-like-the-cached-node-modules)

- [Share variables between jobs](https://stackoverflow.com/questions/52928915/cant-share-global-variable-value-between-jobs-in-gitlab-ci-yaml-file)

- [artifacts:reports:dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)

- https://docs.gitlab.com/ee/ci/yaml/#artifacts

- [GitLab CI: Get source branch after merge-request has been accepted](https://stackoverflow.com/questions/59877180/gitlab-ci-get-source-branch-after-merge-request-has-been-accepted)

- [Export Variables](https://bash.cyberciti.biz/guide/Export_Variables)
