# Screens

In this doc we will present the co-training application and how to use it via a simple workflow.

## Setup the app

To make this demo, i will use the docker compose file to setup the app. Let's start by running the app :

```shell
$ docker-compose up --build
```

And The run google chrome using security mode :

```shell
google-chrome --disable-web-security --user-data-dir=${YOUR_HOME_TMP_DIRECTORY}
```

You can now access the application via :

- http://localhost:5000/signin

## Application users

Here's the default users that you can use to test the app :

| email          | password | roles                                                          |
| -------------- | -------- | -------------------------------------------------------------- |
| admin@mail.com | password | ADMIN                                                          |
| jane@mail.com  | password | TRAININGS_READER, TRAININGS_WRITER, USERS_READER, USERS_WRITER |
| user@mail.com  | password | TRAININGS_READER, TRAININGS_WRITER, USERS_READER, USERS_WRITER |

## Demo

For the demo, i will split it into multiple sections to make it more clear and understandable.

### User subscription

For new users, the first thing to make is to go to the subscribe page using the `SIGN UP` button.

<div align="center">
![1_subscribe](/uploads/73f304996525caf49d20785c7c167dc2/1_subscribe.png)
</div>

The user account will be created but in disable mode. It will be enabled once the admin accepts the this subscription request.

### Admin area

Log into the app using the admin account.

<div align="center">
![2_login](/uploads/d3f2d9be538d78aafcbc4d5cac19624e/2_login.png)
</div>

After logging, you'll be redirected to the `new requests` page.

<div align="center">
![3_new_requests](/uploads/cf364ec0066f38bc11ca37e4a31f81e7/3_new_requests.png)
</div>

As you can see in the screenshot, this page contains the new requests to join the app. In this page you can :

- Enable the user account so he can join the platform

- Change the user roles (by default the user will have the read/write permissions on trainings and users). Check the backend project to know more about roles.

- Disable the alter by clicking on the close button `x`. The account still disabled but the next time we log into the app we will not see this subscription request.

Let's see the admin panels :

<div align="center">
![3_admin_panels](/uploads/4bdaaf24e19473cad9ff38139d095ea3/3_admin_panels.png)
</div>

The users management :

<div align="center">
![4_management](/uploads/5268499f2fb461c86a87a6dacb7719c5/4_management.png)
</div>

Using this page you can delete accounts and manage them by clicking on the `pencil` :

<div align="center">
![4_management_2](/uploads/b9a931d1af3a14f6984b33c0c63083ea/4_management_2.png)
</div>

Now you can :

- update the user resources. resources are number of days and the money that a user can spent on trainings in one year.

- enable and disable accounts

- change the user team

- update the user roles (using drag and drop)

you can update the user resources like the screenshot below :

<div align="center">
![5_update_resources](/uploads/cfec268f60fad0b73b1f26973b5b56b1/5_update_resources.png)
</div>

The team management :

![5_teams](/uploads/e7616eee205103492ec36d93bf646869/5_teams.png)

You can create, update and delete teams using this page.

### Log in using joel account

Logout and login using the last created account.

<div align="center">
![2_login_2](/uploads/578a0df3ce57a5b0326df1c04462ecaf/2_login_2.png)
</div>

You'll be redirected to the home page.

<div align="center">
![6_home](/uploads/a4152a09b3e9efeab9407f4cf31d93ef/6_home.png)
</div>

As you can see in the home page, you'll find the the upcoming trainings and as there is a training given by jane.

1. This is the current user resources

2. This is the user whishlist area

3. Click on this button to participate to the jane's training

4. Click on this button to add the training to your whishlist

5. Click on this button to go to the training details

The user panel are like below :

<div align="center">
![7_user_panel](/uploads/2512ec035fb119f9cc9e5f6c63d50090/7_user_panel.png)
</div>

### The training page

After clicking on the `open_in_nex` button, you'll be redirected to the training details. In this page you can find the training description on the left. On the right, you can find some actions like participate or leave the training.

<div align="center">
![8_training_aside](/uploads/ff9739939070f0976040a31b71d3b0e5/8_training_aside.png)
</div>

You can also add or remove the training from your wshishlist and some important data like the remaining places, participants and the price.

In the bottom, you can add a feedback of the training and you can see other participants feedbacks.

<div align="center">
![10_reviews](/uploads/10cec26542dbb95544e26d82f4ebda2b/10_reviews.png)
</div>

If you add a feedback, you can choose if this feedback will be anonymous or not.

And finally, you can see a brief summary of the training author `jane` :

<div align="center">
![9_training_author](/uploads/22055a9418cb869b17a87d36c628cb5b/9_training_author.png)
</div>

Click on the author `open_in_nex` button to go the author page.

### The author page

In this page, you can see a user summary.

<div align="center">
![11_author_page](/uploads/a915c33cdeca0d0069690cf50958944b/11_author_page.png)
</div>

You can find here the number of trainings given by the author, number of participants and students, the number of reviews and its rating.

If you click on one of the social media links, that link will be opened in a new tab except for `mail` and `phone number`.

<div align="center">
![phone_number](/uploads/f888cdd9da0e4d9bd79c4410e08ccf89/phone_number.png)
</div>

In the bottom, you'll find all the author trainings.

<div align="center">
![11_author_page_2](/uploads/528d17c6ec91eb67a4686ad697f0e8a8/11_author_page_2.png)
</div>

### The resources page

Click on the resources from the header and you'll be redirected to the resources page.

<div align="center">
![12_resources](/uploads/5d3c770c4351d5ca2078ee14cec9ce62/12_resources.png)
</div>

As we have participated to a training, resources have been decreased.

### The whishlist page

Click on the whishlist from the header and you'll be redirected to the whishlist page.

<div align="center">
![13_whishlist](/uploads/b37ad758daacf8f02bb71cd63aec0ba8/13_whishlist.png)
</div>

### My trainings page

From the app menu, go to `My Trainings` to have a look at your trainings.

<div align="center">
![99_participant_jane_author](/uploads/5b93bd00c92ee3d13cef4805ea4c78f5/99_participant_jane_author.png)
</div>

In this page you'll find all the given trainings and also trainings in which you are a participant. As you can see, you are a participant in the `jane vuejs` training. From this page, you can manage you trainings.

If you are the author of the training you'll have something like this (taken from jane account) :

<div align="center">
![99_jane_author](/uploads/fe86e1ad2f06c487b1d22a9e38d1c444/99_jane_author.png)
</div>

If you click on `create`, you'll go to the `create training` tab.

<div align="center">
![15_create_trainings](/uploads/6cf09e97c66a928041d581495ef9ebcf/15_create_trainings.png)
</div>

### About page

The about page contains a brief introduction of the co-training platform, the used tools and technologies and some important links.

<div align="center">
![16_about](/uploads/749d3e71383b2a7af492c07cf1e0d008/16_about.png)
</div>

### Profile page

Go to the profile page :

<div align="center">
![17_person](/uploads/c4b5d1dbe07d633873436031a1616c17/17_person.png)
</div>

From this page you can update you profile. You can change your image url, job, team ...

<div align="center">
![18_profile](/uploads/6a9fc32bf8ce533188aae4965d5c857c/18_profile.png)
</div>

You can also update you social media links so people can contact you. If you suggest that the link is private and not sharable you can make it private by clicking on the little globe on the right or from its image in `1`.

### Settings page

In the settings page, you'll see your account status and your roles. This page provides the ability to update your password, your email and if you want you can delete definitely you account from this page.

<div align="center">
![19_settings](/uploads/68e99ac0db55dde57cd657046e136112/19_settings.png)
</div>

PN : note that the app is configured to logout automatically the user after 1 hour.
