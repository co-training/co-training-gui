# Draw io

We used drawio to create diagrams of the application. You can find in this directory all the app diagrams.

- gitlab pipeline diagram : gitlab-cicd-workflow-frontend.drawio

- kubernetes architecture : kubernetes-architecture.drawio

You can use these files with drawio to adapt the diagrams to your need.
