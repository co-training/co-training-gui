# Docs

This directory contains the application documentation. We will describe the application ci cd process (used gitlab jobs and stages to install the app on the stage platform and then on production), the application screens and how to use and some other features.
Here's the directory structure :

- drawio : the project drawio xml files
- docker-compose.md : in this file we present our docker compose environment
- docker : this file presents and explains the dockerfile used in this app
- gitlab-cicd.md : in this file we will explain our ci/cd pipeline
- pipeline-example.md : in this file we will present how to develop a feature and deploy it on production using the pipeline
- screens.md : this file will help you to understand how to use the app by examples
- render.md : this file explains how to deploy the application demo on render
