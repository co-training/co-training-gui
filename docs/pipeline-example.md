# Pipeline Example

In this readme file we will explain the pipeline used by our project to deploy the application to the production platform. All the jobs has been explained in the
`gitlab-cicd.md` but in this file we will show you a practical case of how to add a new feature until deploy it to the production platform.

## Feature Branch

As explained in our git workflow. We start a new development by creating a new branch feature. In this feature, we will fix some design issues. That's all.

Let's start by creating a new branch from develop :

![1](/uploads/4a72e378a0773829f8f752d4d35ad25a/1.png)

![2](/uploads/6b9110e7b70087b37e023429345897b9/2.png)

I used gitlab to do it but you can use your ide or git commands to do it. After that you can do your changes.

Commit your changes (i'm using visual studio) :

![3](/uploads/c8e0d482d1653105242a7530419e28b9/3.png)

and push them to the repository.

This will trigger the pipeline on gitlab, the jobs that will be executed on a feature branch are **dependencies** and **test**.

![4](/uploads/965135fad310655a6084c3da6529a7a2/4.png)

The first one will download dependencies and second will execute unit tests.

![5](/uploads/4c8c2598154c29a94fd5bcd190711824/5.png)

## Release Milestone

As this is a new release, we will create a new milestone with the release version. It will hold all the merge requests of the release and help us to group them in one location.

Go to the milestones :

![5.1](/uploads/4e8c0c18631c1763105f406b93fa0c9f/5.1.png)

Create a new one with the release version :

![5.2](/uploads/4814816893897e3818937110d4602a7f/5.2.png)

## Merge the feature into develop

Now we can create a merge request to merge the feature into the develop branch.

![6](/uploads/72ef71985065f83349902b6e0ec5c381/6.png)

Introduce the merge request title and description. Don't forget the milestone :

![7](/uploads/96e12e06aceb32a5247e336e25ccc96b/7.png)

Now you can submit the merge request.

You can choose to delete the source branch (feature) and to squash commits.

Double check the details of the merge request. If everything is good, you can click on the merge button.

![8](/uploads/f07d1fadc5871efcc75d24c5cfdb641b/8.png)

This will run the **dependencies** and the **test** jobs.

![9](/uploads/29f75caef1bcf7304c1cbd346d39337d/9.png)

## Release Branch

After merging the feature into develop, we have to merge this last into a release branch. The release branch is used to test the application by the test team.

Checkout a new release branch from master :

![10](/uploads/2c7c5804827d301ad2e3979538659f32/10.png)

As you can see, our release version is `1.0.1`. The release branch name must match the pattern **release/number.number.number**.

Create a merge request from develop :

![11](/uploads/62ef589d05f75832f60a7dbcb2a6927f/11.png)

Don't forget the milestone :

![12](/uploads/bb384358ebbc185b7d4b434ab598f218/12.png)

And merge. In the case you face merge conflicts :

![13](/uploads/bfc1ee438baf505fc55752c1288e1232/13.png)

Follow the procedure in the `Merge locally` button. Resolve conflicts :

![14](/uploads/ee148e8a1007d673886d46fc06be03a0/14.png)

and push you changes.

![15](/uploads/9b23c0c25eee5c3db945f0767b359b9a/15.png)

Don't click on `Merge` just refresh. The pipeline of release branches will be triggered :

![16](/uploads/bf60aa6bbc12884f7d06c60e9e369f7e/16.png)

The **build:quality** will build a new distribution and pass to the next job which is **publish:quality**.

This job will build a docker image with the tag `1.0.1-STAGE` and push it to docker hub.

![17](/uploads/c93b61a8c4628bad4268b3ee019888db/17.png)

The last job will be **deploy:quality** which will deploy the last created docker image to the quality platform.
If you check the deployment from Google Cloud you'll find the last tag :

![18](/uploads/6cad1b9655d4694075abf881fd811e17/18.png)

Now you can access the application on the STAGE platform from :

- http://34.77.177.135/frontend

![18.1](/uploads/57ac6c266acf0a7221ef819a51120399/18.1.png)

![18.2](/uploads/c3d0b8675dc6d15ccd83758f42db1be6/18.2.png)

## Master Branch

Well, after deploying the application to the quality platform, the testing team make all the functional tests and decide to go to production or not. If yes, we
merge this release to the master.

Let's start by creating a merge request (as we can neither merge nor push) :

![19](/uploads/fc11b3ec53668abdc90213b20ae94618/19.png)

Add details and don't forget the milestone :

![20](/uploads/bbbc51d0f422077de87e462c739452da/20.png)

If all is good, we can merge :

![21](/uploads/9278f39e148d111661f6150ee4e2930c/21.png)

The pipeline will be triggered.

![22](/uploads/1b3fbf22724c62dd52e401faf6e900e6/22.png)

As always, **dependencies** and **test** will be executed.

The **build:production** will retrieve the version from git branches and use that version to add a new tag.

![22.1](/uploads/55c97930979ac6a913eb0979537a01e8/22.1.png)

The it will build the distribution and pass it to the next job which is **publish:quality**.

The **publish:quality** job will build two docker images, the first with the tag `1.0.1` and the second with a `latest` tag. It will then push them to docker hub.

![23](/uploads/a7727809ce32cfccc093aecc27971192/23.png)

The last job will be **deploy:quality** which will deploy the last created docker image to the quality platform. As you can see, this job can only be executed manually. So click on the play button to run it.

If you check the deployment from Google Cloud you'll find the last tag :

![24](/uploads/7c83d613ee794785a4242a29d5950443/24.png)

Now you can access the application on the STAGE platform from :

- http://35.241.245.0/frontend

![25](/uploads/4c4f8c099afb9c67650f5234fa55494d/25.png)

![26](/uploads/21cd69276dcb5e31cab43cef4982c57d/26.png)

As I said the milestone holds all the merge requests :

![27](/uploads/4defeae30b289a34cb88237ca647f30a/27.png)
