# Company training GUI

This project provides is an internal enterprise training platform. The idea is to encourage employees to create courses and participate to others given by other employees. The goal is to keep the employees knowledge up to date to maintain a good service quality of the company.
Each employee has a budget of 100\$ per year and can spent them on courses provided by other employees...

This is the frontend part of the project which i built it using vuejs.

The project components :

- UI project: https://gitlab.com/co-training/co-training-gui
- Backend project: https://gitlab.com/co-training/co-training-backend
- Ops project : https://gitlab.com/co-training/co-training-deployment

## Getting started

### Live demo

You can view a live demo over at :

- https://co-training.onrender.com

If you face a connection timeout error, it's normal because the backend application is sleeping as i'm using a free container hosting. Try to reconnect after several seconds and the app will be app. check more details on heroku :

- https://devcenter.heroku.com/articles/free-dyno-hours#dyno-sleeping

To know more about how did i do to deploy the demo on `render` for the frontend and `heroku` for the backend check :

- [Frontend on Render](https://gitlab.com/co-training/co-training-gui/-/blob/master/docs/render.md)

- [Backend on Heroku](https://gitlab.com/co-training/co-training-backend/-/blob/feature/heroku-deployment/docs/heroku.md)

To know more about how to use the app, check this doc :

- https://gitlab.com/co-training/co-training-gui/-/blob/master/docs/screens.md

### Application users

Here's the default users that you can use to test the app :

| email          | password | roles                                                          |
| -------------- | -------- | -------------------------------------------------------------- |
| admin@mail.com | password | ADMIN                                                          |
| jane@mail.com  | password | TRAININGS_READER, TRAININGS_WRITER, USERS_READER, USERS_WRITER |
| user@mail.com  | password | TRAININGS_READER, TRAININGS_WRITER, USERS_READER, USERS_WRITER |

or you can add a new user if your want (use the subscribe page) but you have to accept the request using an administrator account.

## Code structure

Here's the code structure.

```
│
└─── docs                                           : This directory contains the app documentation
│   │
│   │ drawio                                        : This dir contains the drawio graphs of the app
│   │ docker                                        : This doc presents the docker file of the project
│   │ docker-compose.md                             : This doc presents the docker compose file of the project
│   │ gitlab-ci-cd.md                               : This doc presents the pipeline of the project
│   │ screens                                       : This doc will teach you how to use the application
│
└─── k8s                                            : This dir contains the kubernetes config file of the project
│
└─── tests                                          : This directory contains the unit tests of the project
│
└─── nginx                                          : nginx server config - used when building the docker image
│   │
│   └─── nginx.conf                                 : This file contains the config of our nginx server in which we will hold our app
│
└─── .env                                           : This config file contains env variables used in the build process
```

And the sources structure is like below :

```
src
│
└─── assets                                         : Where you put any assets that are imported into your components
│
│
└─── axios                                          : Here we can add axios instances configs
│   │
│   └─── axios-auth.js                              : This file contains the an axios instance to connect to the authorization server and get a token
│
└─── components                                     : All the components of the projects that are not the main views
│   │
│   │ author                                        : This folder contains all parts of the author component (/author)
│   │ common                                        : This folder contains common components
│   │ layout                                        : This contains the layout (toolbar, navigation drawer and the footer) of the app
│   │
│   │ team                                          : This folder contains common components
│   │ │
│   │ └─── TeamSelect.vue                           : A select that will get the teams names when loading this compo
│   │
│   │ training                                      : This folder contains all parts of a training (/training)
│   │
│   │ ui                                            : This folder contains some ui components that are used in other components
│
└─── constants                                      : This folder contains the app constants parameters like urls
│   │
│   │ rest-endpoints.js                             : This file contains the api urls to manage trainings, authors ...
│   │ routes-names.js                               : This file contains the routes names of the app, they are used in the router module
│
└─── mixins                                         : The mixins are the parts of javascript code that is reused in different components
│   │
│   │ author-stats-mixin.js                         : In this mixin we will get the author evaluation
│   │ training-participate-mixin.js                 : This mixin contains methods to participate and leave a training
│   │ training-whishlist-mixin.js                   : This mixin contains methods to add and remove a training from the whishlist
│
└─── plugins                                        : This folder the vuetify configuration
│
└─── router                                         : This folder contains the routes configs of the app
│
└─── store                                          : This contains the vuex store of the app
│   │
│   │ modules                                       : All modules of our store
│   │ │
│   │ └─── authentication.js                        : This store contains the user authentication data like token and some profile data like roles, photo url, name , id ... and some methods like sign, signout and auto-signin
│   │ │
│   │ └─── toolbar.js                               : This store holds two data from the toolbar which are the user whishlist and its resources. Each time the user subscribe to a training or add/remove training to its whishlist this store will be updated
│   │ │
│   │ └─── training.js                              : this store will be used in the training component. Each time we load the component, this store will be changed and the training sub-components will be updated
│   │
│   │ index                                         : This file will merge all the previous modules
│
└─── translations                                   : translation configs (not yet implemented)
│
└─── utils                                          : Some utility files
│
└─── views                                          : This folder contains the routed components of the app
│
│
└─── .env                                           : This file contains the api server urls of the development mode
```

Here is the references that i used to structure the files and the code :

- https://itnext.io/how-to-structure-a-vue-js-project-29e4ddc1aeeb

- https://vueschool.io/articles/vuejs-tutorials/structuring-vue-components/

## Prerequisites

### Node

You will only need Node.js installed on your environment.
You should be able to run the following command after the installation procedure below.

```shell
$ node --version
v10.18.0

$ npm --version
6.13.4
```

you may have different versions.

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.

### Git

Also, be sure to have `git` available in your PATH, `npm` might need it.

### Your favorite IDE

In my case i'm using `Visual Studio Code` but you can use any other ide.

## Installing the project on your IDE

To install the project on your local machine, you have to follow these steps.

1. clone the project

```shell
$ git clone https://gitlab.com/co-training/co-training-gui.git
$ cd co-training-gui
```

2. setup

```shell
$ npm install
```

3. Compiles and hot-reloads for development

```
$ npm run serve
```

### Compiles and minifies for production

To build a distribution for the production, you can use :

```
$ npm run build
```

### Run your unit tests

To run unit tests, you can use :

```
$ npm run test:unit
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Apis

This app uses the rest api provided from the backend project :

- https://gitlab.com/co-training/co-training-backend

So you have to setup the backend first then you can run the gui (graphical user interface) project. Please make sure to disable your browser security as you'll use the frontend and the backend on the same machine otherwise you'll face a cross origin errors.

In my case and as i'm using chrome, i'm using this command to disable the security :

```shell
google-chrome --disable-web-security --user-data-dir=${YOUR_HOME_TMP_DIRECTORY}
```

Please check the .env file before continuing. This file contains the urls of the backend apis. It contains the oauth2 authorization server url and the apis server url (they are on the same physical server in our server. Change values if necessary.

You can access the app via :

- http://localhost:8080/

To have more details about how to use environment variables in vuejs, check these urls :

- https://stackoverflow.com/questions/50828904/using-environment-variables-with-vue-js

- https://cli.vuejs.org/guide/mode-and-env.html#environment-variables

## Run the app on a docker container

You can run this application in a docker image, you can check the steps in the Dockerfile to have more details.

```shell
$ npm run build

$ docker build -t co-training-gui:v0.0.0 .

$ docker container run --rm --name co-training-gui -p 8080:80 co-training-gui:v0.0.0
```

You can access the app via :

- http://localhost:8080/

The nginx server will be loaded but as the backend is not configured, you can't make http calls. We will explain the docker-compose file later in which we will load the frontend and the backend together.

You can find more details about how to dockerize a vue.js app here :

- https://fr.vuejs.org/v2/cookbook/dockerize-vuejs-app.html

## Run the app on a docker compose environment

```yaml
version: '3'

services:
  co-training-rest:
    container_name: co-training-rest-app
    image: mouhamedali/co-training-backend
    environment:
      - SPRING_PROFILES_ACTIVE=h2
      - SERVER_PORT=4000
    ports:
      - 4444:4000

  co-training-gui:
    container_name: co-training-gui-app
    image: mouhamedali/co-training-gui:0.0.1-DEV-SNAPSHOT
    build:
      context: .
      dockerfile: Dockerfile.dev
      args:
        OAUTH_URL: http://localhost:4444/co-training/oauth/token
        APIS_URL: http://localhost:4444/co-training/v1
    environment:
      - NODE_ENV=development
    ports:
      - 5000:80
```

The `docker-compose` contains only two containers :

- The co-training-rest-app container : This container holds the backend project which provides apis for authentication and resources management. We will use the h2 profile which will load the backend using an in-memory database. This will be convenient to test the app. You can find the image docker hub link here https://hub.docker.com/repository/docker/mouhamedali/co-training-backend. You'll have to pull the image before setting up the docker compose environment.

- The co-training-gui-app container. For this container, we will use a different dockerfile from the production file. The file name is `Dockerfile.dev`. In this dockerfile we have two parts. The first one is the build image which is a node image that will setup the apis urls (which are defined in the docker compose) and build the distribution. The second part is the final image which will get the distribution and expose it on an nginx image.

For more details about the dockerfile check the `docs` directory.

```shell
$ docker-compose up --build
```

To access the gui container via sh :

```shell
$ docker exec -it co-training-gui-app sh
```

don't forget to run your browser on disable-security mode :

```shell
$ google-chrome --disable-web-security --user-data-dir=${YOUR_HOME_TMP_DIRECTORY}
```

You can access the app via :

- http://localhost:5000/

![docker-compose-test](/uploads/08b625bf3bd710d5bce59458581a5f4a/docker-compose-test.png)

## Application users

If you are using the demo (backend profile is h2), here's the default users that you can use to test the app :

| email          | password | roles                                                          |
| -------------- | -------- | -------------------------------------------------------------- |
| admin@mail.com | password | ADMIN                                                          |
| jane@mail.com  | password | TRAININGS_READER, TRAININGS_WRITER, USERS_READER, USERS_WRITER |
| user@mail.com  | password | TRAININGS_READER, TRAININGS_WRITER, USERS_READER, USERS_WRITER |

### References

- [Retiring vue-resource](https://medium.com/the-vue-point/retiring-vue-resource-871a82880af4)

- [Double Negation (!!x)](https://riptutorial.com/javascript/example/3047/double-negation----x-)

- [The && and || Operators in JavaScript](https://mariusschulz.com/blog/the-and-and-or-operators-in-javascript)

- [Vue validate](https://logaretm.github.io/vee-validate/)

- [Vue validate : Validation Rules](https://vee-validate.logaretm.com/v2/guide/rules.html)

- [How to Structure a Vue.js Project](https://itnext.io/how-to-structure-a-vue-js-project-29e4ddc1aeeb)

- [When to fetch data in vue js](https://router.vuejs.org/guide/advanced/data-fetching.html#fetching-after-navigation)

- [Code structure vuex](https://vuex.vuejs.org/guide/structure.html)

- [Structure vuejs file](https://github.com/vuejs/vue-hackernews-2.0/blob/master/src/views/ItemView.vue)

- [Vue guards](https://router.vuejs.org/guide/advanced/navigation-guards.html#per-route-guard)

- [Autonamespacing](https://github.com/vuejs/vuex/releases/tag/v2.1.0)

- [Vuex next steps: Namespaces and dynamic modules](https://medium.com/@ariklevliber/vuex-next-steps-namespaces-and-dynamic-modules-92ea23a0ee9a)

- [Axios project](https://github.com/axios/axios)

- [Two-way data binding in Parent and Child component](https://medium.com/@jithilmt/vue-js-2-two-way-data-binding-in-parent-and-child-components-1cd271c501ba)

- [Not proper solution. You should not mutate props. Try this one](https://codesandbox.io/s/vue-template-forked-gjjeg?file=/src/components/SampleComponent.vue:107-114)

- [The correct way to force Vue to re-render a component](https://michaelnthiessen.com/force-re-render/)

- [vue-20-markdown-editor](https://codesandbox.io/embed/github/vuejs/vuejs.org/tree/master/src/v2/examples/vue-20-markdown-editor?codemirror=1&hidedevtools=1&hidenavigation=1&theme=light)

- [HTTP status code for update and delete?](https://stackoverflow.com/questions/2342579/http-status-code-for-update-and-delete)

- [vue.js 2 how to watch store values from vuex](https://stackoverflow.com/questions/43270159/vue-js-2-how-to-watch-store-values-from-vuex)

- [How can I watch synchronously a state change in vuex ?](https://stackoverflow.com/questions/46096261/how-can-i-watch-synchronously-a-state-change-in-vuex)

- [Reactivity in depth](https://vuejs.org/v2/guide/reactivity.html#For-Arrays)

- [Dockerize VueJs app](https://vuejs.org/v2/cookbook/dockerize-vuejs-app.html)

- [Nginx Serving Static Content](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/)

- [Creating NGINX Rewrite Rules](https://www.nginx.com/blog/creating-nginx-rewrite-rules/)

## Authors

- **Amdouni Mohamed Ali** [[github](https://github.com/mouhamed-ali)]

![co-training](/uploads/7a2204f9b5fb7f1671ea0940ff677377/co-training.png)
