# Kubernetes deployment

In this directory, you'll find all the yaml files used to deploy the co-training-gui app to the Google cloud kubernetes service.
This doc does not have all the details, so to install the cloud platform of this project you can follow the steps from this link :

- https://gitlab.com/co-training/co-training-backend/-/tree/master/k8s#kubernetes-deployment

## Architecture

![kubernetes-architecture](/uploads/b0fffcf8854f58781b69300ed96f5b07/kubernetes-architecture.jpg)

As you can see we are using the nginx ingress created to handle all requests to our platform. If the request urls starts with `/backend`, it will be routed to our backend. If it starts with `/frontend`, it will be routed to the frontend.

We are using a ClusterIP and not a NodePort as a service which is more secure. As our application does not need a high performance, we use one replication in the Deployment.
Our pod has been built using only one container, this container holds our vuejs application.

As i said, the full doc can be found in the backend project.
