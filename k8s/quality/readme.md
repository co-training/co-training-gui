# Kubernetes Yaml Files

In this readme file we will explain in details the different kubernetes features that we have used to set up our application on the cloud. We will see these files one by one and we will try to make the explanation simple.

This doc does not contains all the explanations as this is a part of the project. You can find the main part in the backend docs :

- https://gitlab.com/co-training/co-training-backend/-/tree/master/k8s/quality#kubernetes-yaml-files

## Yaml files

### Ingress

`ingress.yaml`

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: co-training-ingress
  namespace: co-training-quality
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  rules:
    - http:
        paths:
          - path: /backend(/|$)(.*)
            backend:
              serviceName: co-training-backend-service
              servicePort: 8080
    - http:
        paths:
          - path: /frontend(/|$)(.*)
            backend:
              serviceName: co-training-frontend-service
              servicePort: 5000
```

Ingress is the component that will intercept the internet traffic and route it to the adequate service. All the traffic (http requests) that start with `/frontend` will be redirected to the backend service.

To make it work, the vuejs app should add the `/frontend` in its http calls to get sources like js and css files. You achieve this by changing the app root context. If you check the `vue.config.js` you'll notice this config :

```js
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/frontend' : '/',
  transpileDependencies: ['vuetify'],
};
```

Which tell vue-cli to change the root path only for a production distribution.

- https://cli.vuejs.org/config/#publicpath

As we are configuring ingress to rewrite the url so we are not needing the rewrite feature defined in the `nginx/nginx.conf` file. Each url that starts with `/frontend/XXX` will be replaces with `/XXX`.

### Service

`service.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: co-training-frontend-service
  namespace: co-training-quality
spec:
  selector:
    component: co-training
    type: frontend
  type: ClusterIP
  ports:
    - port: 5000
      targetPort: 80
```

The ClusterIP is the default service used by kubernetes. It exposes the Service on an internal IP in the cluster.
This type makes the Service only reachable from within the cluster. This service intercepts requests on the port 5000 and send them to the app on the port 80. Check the docker image and you'll find that our image uses an nginx that listen to connections on the port 80.

### Deployment

`deployment.yaml`

I will split this file and explain each section apart to make it understandable.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: co-training-frontend-deployment
  namespace: co-training-quality
spec:
  replicas: 1
  selector:
    matchLabels:
      component: co-training
      type: frontend
  template:
    metadata:
      labels:
        component: co-training
        type: frontend
    spec:
      containers:
        - name: co-training
          image: mouhamedali/co-training-gui:$BUILD_VERSION
          imagePullPolicy: Always
          ports:
            - containerPort: 80
          readinessProbe:
            httpGet:
              path: /frontend/healthz
              port: 80
          livenessProbe:
            httpGet:
              path: /frontend/healthz
              port: 80
```

Now we can see the configuration of our application container. As you can see we are using the docker with a tag defined as an environment variable. The pipeline is the responsible of changing this data to the version to be deployed to the quality or the production environment.

Liveness and readiness probes used to check if the application is still alive to handle a new request and if it is ready to accept traffic.

- [Configure Liveness, Readiness and Startup Probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes)
