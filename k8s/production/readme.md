# Kubernetes Yaml Files

This directory contains the yaml kubernetes files to be deployed to the production platform. A full explanation of these file has been provided in the `k8s/quality` directory.
The only difference between these files is the used namespace.
