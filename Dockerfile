FROM nginx:1.18.0-alpine

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# copy distribution files
COPY ./dist /usr/share/nginx/html

# copy the nginx conf
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

# exopse the app on the port 80
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]